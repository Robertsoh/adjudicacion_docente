Instalación
===========

- Configuración dentro de Django: `settings/base.py`

* `DB_NAME` nombre de la base de datos
* `DB_USER` usuario de la base de datos
* `DB_HOST` servidor de base de datos
* `DB_PORT` puerto de la base de datos
* `SECRET_KEY` Secret Key de Django
* `GA_CODE` Google Analytics code

- Cargar data:

```
    python manage.py loaddata apps/colegios/fixtures/colegios.json
```

Docker
======

docker-compose run web python manage.py migrate

docker-compose run web python manage.py loaddata apps/colegios/fixtures/colegios.json

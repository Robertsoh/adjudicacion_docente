FROM python:3.6

WORKDIR /app

RUN apt-get update && apt-get install build-essential python3-dev libpq-dev libxslt-dev gettext -y

COPY . /app

RUN pip install -r requirements/prod.txt

EXPOSE 8000

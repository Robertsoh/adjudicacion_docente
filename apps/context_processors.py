import os


def base_data(request):
    return {
        'GA_CODE': os.environ.get('GA_CODE', '')
    }
